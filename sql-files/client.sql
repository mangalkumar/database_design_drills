CREATE TABLE clients(
	id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(100),
    location VARCHAR(100)
);

CREATE TABLE managers(
	id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(100),
    location VARCHAR(100)
);

CREATE TABLE contracts(
	id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(100),
    estimated_cost INT,
    completion_date date,
    client_id INT REFERENCES clients(id),
    manager_id INT REFERENCES managers(id)
);

CREATE TABLE staffs(
	id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(100),
    location VARCHAR(100)
);

CREATE TABLE contracts_with_staffs(
	contract_id INT REFERENCES contracts(id),
	staff_id INT REFERENCES staffs(id)
);