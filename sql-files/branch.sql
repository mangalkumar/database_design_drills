CREATE TABLE publishers(
	id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(100)
);

CREATE TABLE branches(
	id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(100),
    address VARCHAR(300),
    publisher_id INT REFERENCES publishers(id)
);

CREATE TABLE authors(
	id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(100)
);

CREATE TABLE books(
	id INT AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(100),
    isbn VARCHAR(50),
    branch_id INT REFERENCES branches(id),
    no_of_copies INT
);

CREATE TABLE books_with_authors(
	book_id INT REFERENCES books(id),
	author_id INT REFERENCES authors(id)
);