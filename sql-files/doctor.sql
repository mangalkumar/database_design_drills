CREATE TABLE patients(
	id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(100),
    dob date,
    address VARCHAR(300)
);

CREATE TABLE doctors(
	id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(100)
);

CREATE TABLE secretaries(
	id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(100)
);

CREATE TABLE prescriptions(
	id INT AUTO_INCREMENT PRIMARY KEY,
    issue_Date date,
    dosage INT,
    doctor_id INT REFERENCES doctors(id)
);

CREATE TABLE drugs(
	id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(100)
);

CREATE TABLE doctors_with_secretaries(
	doctor_id INT REFERENCES doctors(id),
	secretary_id INT REFERENCES secretaries(id)
);

CREATE TABLE doctors_with_patients(
	doctor_id INT REFERENCES doctors(id),
	patient_id INT REFERENCES patients(id)
);

CREATE TABLE prescriptions_with_drugs(
	prescription_id INT REFERENCES prescriptions(id),
	drug_id INT REFERENCES drugs(id)
);

CREATE TABLE patients_with_prescriptions(
	patient_id INT REFERENCES patients(id),
	prescription_id INT REFERENCES prescriptions(id)
);